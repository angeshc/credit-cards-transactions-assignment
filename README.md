# Transaction Listing Page Assignment
## Link To Prototype: https://xd.adobe.com/view/4b47994b-204e-4287-bd4e-92057983aff0-1e14/ ##

# Create

* which will have a list of scrollable card, the one in front is the selected one.(you can have a drop down as an alternative if you got no time)
* The transaction listed below will be based on the card number selected above
* Only the recent 5 transactions will be listed in the list
* It will also have a show more button or link if the above selected card has more transactions than 5 transactions.
* On click on view more redirect to another page with all the transaction and label of the selected card number on top of it
* Make sure the list is in descending order. That means the latest transaction should be on top.
* The list will have a merchant name, date of transaction, amount of transaction, category it belongs to like shopping, investment, bills etc.

# GUIDELINES

* Running npm install && npm start should be the only command required after cloning to demo your app.
* Use only React/Redux, Webpack, SCSS/LESS(CSS3), HTML5 and anymore related UI tool.
* Make some appropriate creative choices about how to implement and style the web tool
* Loading the splashbase api results should be asynchronous and display a loading or searching visual.
* Editing the caption text should appear on the image as it is typed.
* Error should be handled gracefully.
* Write Unit Test for the components and functionalities using Jest and Enzyme.
* Try to optimise the build.
* Documentation including the details of app and its components.
* Host it using any free hosting service.(optional)
* The app should be PWA (optional)

# Who do I talk to?

* You could either download or fork this repository
* Create a pull request when done

# FAQ
* Can I use Angular/VueJS framework here?
  * No, the application as mentioned  is needed to be in ReactJS.
* Can I use insert library here?
  * If it can be installed via npm you can use it!
* Do I need to unit/functional test or lint my code?
  * It is not required, but linting and tests are always great! Primary focus should be the web tool.
* Do I need to add continuous integration?
  * You are welcome to set up travis, circleci, appveyor, etc... if you want to, but it is not required
* Do I need to fully document my code?
  * Document the code as if you were submitting it for a code review
* Can I go beyond the guidelines and implement more?
  * Of course! just make sure you have the basic pieces completed, but we're happy to see how you can get creative with this!